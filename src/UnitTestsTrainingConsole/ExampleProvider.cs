﻿using System.Drawing;
using UnitTestsTraining.Canvas;

namespace UnitTestsTrainingConsole
{
    internal class ExampleProvider
    {
        private readonly ICanvas _canvas;

        public ExampleProvider(ICanvas canvas)
        {
            _canvas = canvas;
        }

        public void DrawExamplesOnCanvas()
        {
            _canvas.StartDrawing();

            _canvas.DrawPixel(Color.Red, new Point(50, 124));
            _canvas.DrawLine(Color.Blue, new Point(0, 0), new Point(_canvas.Width, _canvas.Height));

            _canvas.FillRectangle(Color.Yellow, new Point(500, 300), new Size(150, 100));
            _canvas.FillPolygon(Color.Magenta, new[] { new Point(180, 180), new Point(350, 230), new Point(230, 230) });

            _canvas.EndDrawing();
        }
    }
}
