﻿using UnitTestsTraining.Canvas;

namespace UnitTestsTrainingConsole
{
    internal class Bootstrapper
    {
        private const int WIDTH = 800;
        private const int HEIGHT = 640;

        private const string OUTPUT_FILE_PATH = "out.bmp";

        public ICanvas Canvas { get; set; }

        public Bootstrapper()
        {
            Load();
        }

        private void Load()
        {
            Canvas = new BitmapFileCanvas(OUTPUT_FILE_PATH, WIDTH, HEIGHT);
        }
    }
}
