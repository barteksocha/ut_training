﻿using System;
using System.Reflection;
using UnitTestsTraining.Figures;

namespace UnitTestsTrainingConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Star star = new Star();
            PropertyInfo[] props = typeof(Star).GetProperties();
            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
            }

            if (args.Length < 1)
            {
                Console.WriteLine("Not enough parameters");
                return;
            }

            Bootstrapper bootstrapper = new Bootstrapper();
            ExampleProvider exampleProvider = new ExampleProvider(bootstrapper.Canvas);

            if (args[0] == "0")
            {
                exampleProvider.DrawExamplesOnCanvas();
                Console.WriteLine("Using base library only with canvas to draw some simple shapes, output file: out.bmp");
            }
            else
            {
                Console.WriteLine("Not proper parameter");
            }
        }
    }
}
