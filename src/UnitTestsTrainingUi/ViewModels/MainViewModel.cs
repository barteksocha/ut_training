﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using System.Windows.Shapes;

using UnitTestsTraining.Figures;
using UnitTestsTraining.Figures.Managing;
using UnitTestsTrainingUi.Utilities;

namespace UnitTestsTrainingUi.ViewModels
{
    internal class MainViewModel : BaseViewModel
    {
        private Bitmap _bitmapImage;

        private List<IFigure> _figuresList;
        private IFigure _selectedFigure;

        public MainViewModel()
        {
            AvailableFigureTypes = new FigureTypesProvider().GetFigureTypes();
            _figuresList = new List<IFigure>();
        }

        public Dictionary<string, Type> AvailableFigureTypes { get; }

        public ICommand OnFigureAddedCommand => new RelayCommand(OnFigureAdded);
        public ICommand OnFigureDeletedCommand => new RelayCommand(OnFigureDeleted);
        public ICommand OnFigureMovedUpCommand => new RelayCommand(OnFigureMovedUp, CanMoveUpFigure);
        public ICommand OnFigureMovedDownCommand => new RelayCommand(OnFigureMovedDown, CanMoveDownFigure);
        public ICommand OnRedrawCommand => new RelayCommand(OnContentChanged);
        public ICommand OnContentChangedCommand => new RelayCommand(OnContentChanged);

        public List<IFigure> FiguresList
        {
            get => _figuresList;
            set
            {
                _figuresList = value;
                OnPropertyChanged();
            }
        }

        public List<Shape> Shapes => new List<Shape>(new [] {new Line {X1= 0.0, Y1 = 200.0, X2 = 200.0, Y2 = 0.0, Stroke = System.Windows.Media.Brushes.Red, StrokeThickness = 3.0}});

        public IFigure SelectedFigure
        {
            get => _selectedFigure;
            set
            {
                _selectedFigure = value;
                OnPropertyChanged();
            }
        }

        public Bitmap BitmapImage
        {
            get => _bitmapImage;
            set
            {
                _bitmapImage = value;
                OnPropertyChanged();
            }
        }

        private void OnFigureAdded(object obj)
        {
            if (obj is Type type)
            {
                IFigure newfigure = ((IFigure)Activator.CreateInstance(type));
                newfigure.Name = $"{type.Name} {1 + FiguresList.Count(f => f.GetType() == type)}";
                FiguresList.Add(newfigure);
                FiguresList = new List<IFigure>(FiguresList);
            }
        }

        private void OnFigureDeleted(object obj)
        {
            if (obj is IFigure figure)
            {
                FiguresList.Remove(figure);
                FiguresList = new List<IFigure>(FiguresList);
            }
        }

        private void OnFigureMovedUp(object obj)
        {
            if (obj is IFigure figure)
            {
                int currentFigureIndex = FiguresList.IndexOf(figure);
                FiguresList.RemoveAt(currentFigureIndex);
                FiguresList.Insert(currentFigureIndex - 1, figure);

                FiguresList = new List<IFigure>(FiguresList);
            }
        }

        private void OnFigureMovedDown(object obj)
        {
            if (obj is IFigure figure)
            {
                int currentFigureIndex = FiguresList.IndexOf(figure);
                FiguresList.RemoveAt(currentFigureIndex);
                FiguresList.Insert(currentFigureIndex + 1, figure);

                FiguresList = new List<IFigure>(FiguresList);
            }
        }

        private void OnContentChanged(object obj)
        {
            FiguresList = new List<IFigure>(FiguresList);
            OnPropertyChanged(nameof(FiguresList));
        }

        private bool CanMoveDownFigure(object arg)
        {
            if (arg is IFigure figure)
            {
                return FiguresList.IndexOf(figure) < FiguresList.Count - 1;
            }

            return false;
        }

        private bool CanMoveUpFigure(object arg)
        {
            if (arg is IFigure figure)
            {
                return FiguresList.IndexOf(figure) > 0;
            }

            return false;
        }
    }
}
