﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UnitTestsTraining.Canvas;
using UnitTestsTraining.Drawing;
using UnitTestsTraining.Figures;

namespace UnitTestsTrainingUi.Converters
{
    internal class FigureListToBitmapConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is List<IFigure> figuresList)
            {
                Bitmap bitmapImage = new Bitmap(800, 600, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                IFigureProvider figureProvider = new FigureProvider(figuresList);
                ICanvas bitmapCanvas = new BitmapImageCanvas(bitmapImage);
                IDrawer drawer = new Drawer(figureProvider, bitmapCanvas);

                drawer.DrawFigures();

                BitmapData bitmapData = bitmapImage.LockBits(
                    new Rectangle(0, 0, bitmapImage.Width, bitmapImage.Height), ImageLockMode.ReadOnly, bitmapImage.PixelFormat);

                BitmapSource bitmapSource = BitmapSource.Create(
                    bitmapData.Width,
                    bitmapData.Height,
                    bitmapImage.HorizontalResolution,
                    bitmapImage.VerticalResolution,
                    PixelFormats.Bgr24,
                    null,
                    bitmapData.Scan0,
                    bitmapData.Stride * bitmapData.Height,
                    bitmapData.Stride);

                return bitmapSource;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
