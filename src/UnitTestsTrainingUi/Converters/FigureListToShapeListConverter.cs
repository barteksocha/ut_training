﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Shapes;

using UnitTestsTraining.Canvas;
using UnitTestsTraining.Drawing;
using UnitTestsTraining.Figures;

namespace UnitTestsTrainingUi.Converters
{
    internal class FigureListToShapeListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is List<IFigure> figuresList)
            {
                List<Shape> shapesList = new List<Shape>();

                IFigureProvider figureProvider = new FigureProvider(figuresList);
                ICanvas bitmapCanvas = new WindowsShapeCanvas(shapesList);
                IDrawer drawer = new Drawer(figureProvider, bitmapCanvas);

                drawer.DrawFigures();

                return shapesList;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
