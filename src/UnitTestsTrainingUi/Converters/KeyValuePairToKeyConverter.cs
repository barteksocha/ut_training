﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace UnitTestsTrainingUi.Converters
{
    internal class KeyValuePairToKeyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Type valueType = value.GetType();
            if (valueType.IsGenericType && valueType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
            {
                var valueAsKeyValuePair = (dynamic)value;
                return valueAsKeyValuePair.Key;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
