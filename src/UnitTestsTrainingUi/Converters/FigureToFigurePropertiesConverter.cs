﻿using System;
using System.Globalization;
using System.Windows.Data;
using UnitTestsTraining.Figures;
using UnitTestsTraining.Figures.Attributes;

namespace UnitTestsTrainingUi.Converters
{
    internal class FigureToFigurePropertiesConverter : IValueConverter
    {
        private readonly IFigurePropertyFactory _figurePropertyFactory;

        public FigureToFigurePropertiesConverter()
        {
            _figurePropertyFactory = new FigurePropertyFactory();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IFigure figure)
            {
                return _figurePropertyFactory.GetFigureProperties(figure);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
