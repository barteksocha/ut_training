﻿using System.Runtime.Remoting;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace UnitTestsTrainingUi.UserControls
{
    internal class ExtendedTextBox : TextBox
    {
        private bool _lostFocusHandlerAdded = false;

        public static readonly DependencyProperty TextChangedCommandProperty = DependencyProperty.RegisterAttached(
            nameof(TextChangedCommand),
            typeof(ICommand),
            typeof(ExtendedTextBox),
            new PropertyMetadata(default(ICommand), TextChangedCommandChangedHandler));

        public ICommand TextChangedCommand
        {
            get => (ICommand)GetValue(TextChangedCommandProperty);
            set => SetValue(TextChangedCommandProperty, value);
        }

        public void AddLostFocusHandler()
        {
            if (!_lostFocusHandlerAdded)
            {
                LostFocus += OnLostFocus;
                _lostFocusHandlerAdded = true;
            }
        }

        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            TextChangedCommand?.Execute(null);
        }

        public object OnPropertyChangedHandler { get; set; }

        private static void TextChangedCommandChangedHandler(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            if (dependencyObject is ExtendedTextBox textBox)
            {
                textBox.AddLostFocusHandler();
            }
        }
    }
}
