﻿using WindowsPoint = System.Windows.Point;
using DrawingPoint = System.Drawing.Point;

namespace UnitTestsTraining.Essentials.Extensions
{
    internal static class WindowsDrawingPointsConverter
    {
        public static WindowsPoint ToWindowsPoint(this DrawingPoint point)
        {
            return new WindowsPoint(point.X, point.Y);
        }
    }
}
