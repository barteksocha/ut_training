﻿using MediaColor = System.Windows.Media.Color;
using DrawingColor = System.Drawing.Color;

namespace UnitTestsTraining.Essentials.Extensions
{
    internal static class MediaDrawingColorsConverter
    {
        public static MediaColor ToMediaColor(this DrawingColor drawingColor)
        {
            return MediaColor.FromArgb(drawingColor.A, drawingColor.R, drawingColor.G, drawingColor.B);
        }
    }
}
