﻿namespace UnitTestsTraining.Drawing
{
    public interface IDrawer
    {
        void DrawFigures();
    }
}
