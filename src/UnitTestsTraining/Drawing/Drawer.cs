﻿using UnitTestsTraining.Canvas;
using UnitTestsTraining.Figures;

namespace UnitTestsTraining.Drawing
{
    public class Drawer : IDrawer
    {
        private readonly IFigureProvider _figureProvider;
        private readonly ICanvas _canvas;

        public Drawer(IFigureProvider figureProvider, ICanvas canvas)
        {
            _figureProvider = figureProvider;
            _canvas = canvas;
        }

        public void DrawFigures()
        {
            while (_figureProvider.IsNextFigureAvailable)
            {
                _figureProvider.GetNextFigure().DrawFigure(_canvas);
            }
        }
    }
}
