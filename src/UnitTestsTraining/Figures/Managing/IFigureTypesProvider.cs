﻿using System;
using System.Collections.Generic;

namespace UnitTestsTraining.Figures.Managing
{
    public interface IFigureTypesProvider
    {
        Dictionary<string, Type> GetFigureTypes();
    }
}
