﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UnitTestsTraining.Figures.Managing
{
    public class FigureTypesProvider : IFigureTypesProvider
    {
        public Dictionary<string, Type> GetFigureTypes()
        {
            return Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => typeof(IFigure).IsAssignableFrom(t))
                .Where(t => t != typeof(IFigure))
                .ToDictionary(t => t.Name, t => t);
        }
    }
}
