﻿using System.Collections.Generic;

namespace UnitTestsTraining.Figures
{
    public class FigureProvider : IFigureProvider
    {
        private readonly Stack<IFigure> _figuresStack;

        public FigureProvider()
        {
            _figuresStack = new Stack<IFigure>();
        }

        public FigureProvider(List<IFigure> figuresList)
        {
            _figuresStack = new Stack<IFigure>(figuresList);
        }

        public bool IsNextFigureAvailable => _figuresStack.Count > 0;

        public void AddFigureToDraw(IFigure figure)
        {
            _figuresStack.Push(figure);
        }

        public IFigure GetNextFigure()
        {
            return _figuresStack.Pop();
        }
    }
}
