﻿namespace UnitTestsTraining.Figures
{
    public interface IFigureProvider
    {
        bool IsNextFigureAvailable { get; }

        IFigure GetNextFigure();

        void AddFigureToDraw(IFigure figure);
    }
}
