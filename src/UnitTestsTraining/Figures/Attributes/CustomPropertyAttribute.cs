﻿using System;
using System.Collections.Generic;
using UnitTestsTraining.Figures.Attributes.Converters;
using UnitTestsTraining.Figures.Attributes.Validators;

namespace UnitTestsTraining.Figures.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    internal class CustomPropertyAttribute : Attribute
    {
        public CustomPropertyAttribute(
            string name,
            params object[] parameters)
        {
            Name = name;
            Validators = new List<KeyValuePair<ValidatorType, List<object>>>();

            ParseParameters(parameters);
        }

        private enum ParsingState
        {
            Unknown = 0,
            ParsingValidator,
            TypeIndicatorParsing
        }

        private void ParseParameters(object[] parameters)
        {
            ParsingState parsingState = ParsingState.TypeIndicatorParsing;

            int index = 0;

            while (index < parameters.Length)
            {
                switch (parsingState)
                {
                    case ParsingState.TypeIndicatorParsing:
                        ParseTypeIndicator(parameters[index], ref parsingState, ref index);
                        break;

                    case ParsingState.ParsingValidator:
                        ParseValidatorParameters(parameters[index], ref parsingState, ref index);
                        break;
                }
            }
        }

        private bool IsConverterTypeParameter(object parameter)
        {
            return parameter is ConverterType;
        }

        private bool IsValidatorTypeParameter(object parameter)
        {
            return parameter is ValidatorType;
        }

        private bool IsTypeIndicatorParameter(object parameter)
        {
            return IsConverterTypeParameter(parameter) || (IsValidatorTypeParameter(parameter));
        }

        private void ParseValidatorParameters(object parameter, ref ParsingState parsingState, ref int index)
        {
            if (IsTypeIndicatorParameter(parameter))
            {
                parsingState = ParsingState.TypeIndicatorParsing;
            }
            else
            {
                Validators[Validators.Count - 1].Value.Add(parameter);
                ++index;
            }
        }

        private void ParseTypeIndicator(object parameter, ref ParsingState parsingState, ref int index)
        {
            if (IsConverterTypeParameter(parameter))
            {
                Converter = (ConverterType)parameter;
            }
            else if (IsValidatorTypeParameter(parameter))
            {
                Validators.Add(new KeyValuePair<ValidatorType, List<object>>((ValidatorType)parameter, new List<object>()));
                parsingState = ParsingState.ParsingValidator;
            }

            ++index;
        }

        public string Name { get; private set; }
        public ConverterType Converter { get; private set; }
        public List<KeyValuePair<ValidatorType, List<object>>> Validators { get; private set; }
    }
}
