﻿using System.Collections.Generic;

namespace UnitTestsTraining.Figures.Attributes.Validators
{
    public interface IPropertyValidator
    {
        void Initialize(List<object> parameters);
        object ValidatePropertyValue(object inputValue);
    }
}
