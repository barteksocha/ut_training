﻿using System;
using System.Collections.Generic;

namespace UnitTestsTraining.Figures.Attributes.Validators
{
    [Validator(ValidatorType.MinMaxValidator)]
    internal class MinMaxValidator : IPropertyValidator
    {
        private const int MIN_VALUE_PARAMETER_INDEX = 0;
        private const int MAX_VALUE_PARMAETER_INDEX = 1;

        private IComparable _minValue;
        private IComparable _maxValue;

        public object ValidatePropertyValue(object inputValue)
        {
            if (inputValue is IComparable comparableInput)
            {
                if (_minValue != null && _minValue.GetType() == inputValue.GetType() && _minValue.CompareTo(comparableInput) > 0)
                {
                    inputValue = _minValue;
                }

                if (_maxValue != null && _maxValue.GetType() == inputValue.GetType() && _maxValue.CompareTo(comparableInput) < 0)
                {
                    inputValue = _maxValue;
                }
            }

            return inputValue;
        }

        public void Initialize(List<object> parameters)
        {
            _minValue = null;
            _maxValue = null;

            if (parameters.Count > MIN_VALUE_PARAMETER_INDEX && parameters[MIN_VALUE_PARAMETER_INDEX] is IComparable newMinValue)
            {
                _minValue = newMinValue;
            }

            if (parameters.Count > MAX_VALUE_PARMAETER_INDEX && parameters[MAX_VALUE_PARMAETER_INDEX] is IComparable newMaxValue)
            {
                _maxValue = newMaxValue;
            }

            if (_minValue != null && _maxValue != null && _minValue.GetType() != _maxValue.GetType())
            {
                _maxValue = null;
            }
        }
    }
}
