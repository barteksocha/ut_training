﻿using System;

namespace UnitTestsTraining.Figures.Attributes.Validators
{
    internal class ValidatorAttribute : Attribute
    {
        public ValidatorAttribute(ValidatorType type)
        {
            Type = type;
        }

        public ValidatorType Type { get; private set; }
    }
}
