﻿namespace UnitTestsTraining.Figures.Attributes.Converters
{
    [Converter(ConverterType.String)]
    class StringConverter : IPropertyConverter
    {
        public bool Convert(string value, out object result)
        {
            result = value;
            return true;
        }

        public string ConvertToString(object value)
        {
            return value.ToString();
        }
    }
}
