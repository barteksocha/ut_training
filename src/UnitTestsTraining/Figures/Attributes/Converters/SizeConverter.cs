﻿using System.Drawing;

namespace UnitTestsTraining.Figures.Attributes.Converters
{
    [Converter(ConverterType.Size)]
    internal class SizeConverter : IntegerVectorConverter
    {
        private const int VALUES_COUNT = 2;

        public SizeConverter()
            : base(VALUES_COUNT)
        { }

        public override bool Convert(string value, out object result)
        {
            bool returnValue = base.Convert(value, out object baseResult);

            result = null;
            if (returnValue && baseResult is int[] baseVector)
            {
                result = new Size(baseVector[0], baseVector[1]);
            }

            return returnValue;
        }

        public override string ConvertToString(object value)
        {
            if (value is Size size)
            {
                return base.ConvertToString(new[] { size.Width, size.Height });
            }
            return string.Empty;
        }
    }
}
