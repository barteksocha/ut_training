﻿namespace UnitTestsTraining.Figures.Attributes.Converters
{
    [Converter(ConverterType.Integer)]
    internal class IntegerConverter : IPropertyConverter
    {
        public bool Convert(string value, out object result)
        {
            result = null;
            if (int.TryParse(value, out int returnValue))
            {
                result = returnValue;
                return true;
            }
            else
            {
                return false;
            }
        }

        public string ConvertToString(object value)
        {
            if (value is int valueAsInt)
            {
                return valueAsInt.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
