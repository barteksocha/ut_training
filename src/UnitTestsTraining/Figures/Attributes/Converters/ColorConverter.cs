﻿using System.Drawing;

namespace UnitTestsTraining.Figures.Attributes.Converters
{
    [Converter(ConverterType.Color)]
    internal class ColorConverter : IntegerVectorConverter
    {
        private const int MAX_CHANEL_VALUES = 3;

        public ColorConverter()
            : base(MAX_CHANEL_VALUES)
        { }

        public override bool Convert(string value, out object result)
        {
            bool returnValue = base.Convert(value, out object baseResult);

            result = null;
            if (returnValue && baseResult is int[] baseVector)
            {
                result = Color.FromArgb(baseVector[0], baseVector[1], baseVector[2]);
            }
            
            return returnValue;
        }

        public override string ConvertToString(object value)
        {
            if (value is Color color)
            {
                return base.ConvertToString(new int[] { color.R, color.G, color.B });
            }
            return string.Empty;
        }
    }
}
