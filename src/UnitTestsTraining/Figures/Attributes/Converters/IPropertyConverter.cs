﻿namespace UnitTestsTraining.Figures.Attributes.Converters
{
    public interface IPropertyConverter
    {
        bool Convert(string value, out object result);
        string ConvertToString(object value);
    }
}
