﻿namespace UnitTestsTraining.Figures.Attributes.Converters
{
    internal enum ConverterType
    {
        Integer,
        Color,
        Point,
        Size,
        Float,
        String
    }
}
