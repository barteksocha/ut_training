﻿using System;

namespace UnitTestsTraining.Figures.Attributes.Converters
{
    internal class ConverterAttribute : Attribute
    {
        public ConverterAttribute(ConverterType type)
        {
            Type = type;
        }

        public ConverterType Type { get; private set; }
    }
}
