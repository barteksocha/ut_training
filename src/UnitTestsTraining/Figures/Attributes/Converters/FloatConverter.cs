﻿using System.Globalization;

namespace UnitTestsTraining.Figures.Attributes.Converters
{
    [Converter(ConverterType.Float)]
    internal class FloatConverter : IPropertyConverter
    {
        public bool Convert(string value, out object result)
        {
            result = null;
            if (float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out float returnValue))
            {
                result = returnValue;
                return true;
            }
            else
            {
                return false;
            }
        }

        public string ConvertToString(object value)
        {
            if (value is float valueAsFloat)
            {
                return valueAsFloat.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
