﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTestsTraining.Figures.Attributes.Converters
{
    internal class IntegerVectorConverter : IPropertyConverter
    {
        private readonly int _valuesCount;

        public IntegerVectorConverter(int valuesCount)
        {
            _valuesCount = valuesCount;
        }

        public virtual bool Convert(string value, out object result)
        {
            result = null;

            List<string> rawValues = value.Split(',').ToList();
            if (rawValues.Count > _valuesCount)
            {
                return false;
            }

            int[] values = new int[_valuesCount];
            for (int i = 0; i < _valuesCount; ++i)
            {
                values[i] = 0;
            }

            for (int i = 0; i < rawValues.Count; ++i)
            {
                if (!int.TryParse(rawValues[i], out int currentValue))
                {
                    return false;
                }

                values[i] = currentValue;
            }

            result = values;
            return true;
        }

        public virtual string ConvertToString(object value)
        {
            if (value is int[] valueAsArray)
            {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < valueAsArray.Length; ++i)
                {
                    stringBuilder.Append(valueAsArray[i]);

                    if (i < valueAsArray.Length - 1)
                    {
                        stringBuilder.Append(", ");
                    }
                }

                return stringBuilder.ToString();
            }
            return string.Empty;
        }
    }
}
