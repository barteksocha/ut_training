﻿using System.Drawing;

namespace UnitTestsTraining.Figures.Attributes.Converters
{
    [Converter(ConverterType.Point)]
    internal class PointConverter : IntegerVectorConverter
    {
        private const int VALUES_COUNT = 2;

        public PointConverter()
            : base(VALUES_COUNT)
        {
        }

        public override bool Convert(string value, out object result)
        {
            bool returnValue = base.Convert(value, out object baseResult);

            result = null;
            if (returnValue && baseResult is int[] baseVector)
            {
                result = new Point(baseVector[0], baseVector[1]);
            }

            return returnValue;
        }

        public override string ConvertToString(object value)
        {
            if (value is Point point)
            {
                return base.ConvertToString(new[] { point.X, point.Y });
            }
            return string.Empty;
        }
    }
}
