﻿using System.Collections.Generic;

namespace UnitTestsTraining.Figures.Attributes
{
    public interface IFigurePropertyFactory
    {
        List<IFigureProperty> GetFigureProperties(IFigure figure);
    }
}
