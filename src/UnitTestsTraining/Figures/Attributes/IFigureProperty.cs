﻿namespace UnitTestsTraining.Figures.Attributes
{
    public interface IFigureProperty
    {
        string Name { get; }
        string Value { get; set; }
    }
}
