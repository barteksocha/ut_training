﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnitTestsTraining.Figures.Attributes.Converters;
using UnitTestsTraining.Figures.Attributes.Validators;

namespace UnitTestsTraining.Figures.Attributes
{
    public class FigurePropertyFactory : IFigurePropertyFactory
    {
        public List<IFigureProperty> GetFigureProperties(IFigure figure)
        {
            List<IFigureProperty> result = new List<IFigureProperty>();
            result.Add(GetFigureProperty(figure, ConverterType.Color, nameof(figure.BorderColor)));
            result.Add(GetFigureProperty(figure, ConverterType.Color, nameof(figure.FillColor)));
            result.Add(GetFigureProperty(figure, ConverterType.Size, nameof(figure.Size)));
            result.Add(GetFigureProperty(figure, ConverterType.Point, nameof(figure.Position)));
            result.Add(GetFigureProperty(figure, ConverterType.String, nameof(figure.Name)));

            List<PropertyInfo> customPropertiesList = figure
                .GetType()
                .GetProperties()
                .Where(pi => Attribute.GetCustomAttribute(figure.GetType().GetProperty(pi.Name), typeof(CustomPropertyAttribute)) != null)
                .ToList();

            foreach (PropertyInfo propertyInfo in customPropertiesList)
            {
                result.Add(GetCustomProperty(figure, propertyInfo));
            }

            return result;
        }

        private IFigureProperty GetCustomProperty(IFigure figure, PropertyInfo propertyInfo)
        {
            CustomPropertyAttribute customPropertyAttribute = (CustomPropertyAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CustomPropertyAttribute));
            if (customPropertyAttribute != null)
            {
                return new FigureProperty(
                    figure,
                    GetConverter(customPropertyAttribute.Converter),
                    GetValidators(customPropertyAttribute.Validators).ToArray(),
                    propertyInfo.Name);
            }

            return null;
        }

        private IFigureProperty GetFigureProperty(IFigure figure, ConverterType converterType, string propertyName)
        {
            IPropertyConverter propertyConverter = GetConverter(converterType);
            return new FigureProperty(figure, propertyConverter, new IPropertyValidator[0], propertyName);
        }

        private List<IPropertyValidator> GetValidators(List<KeyValuePair<ValidatorType, List<object>>> validatorsData)
        {
            return validatorsData
                .Select(vd => GetValidator(vd.Key, vd.Value))
                .ToList();
        }

        private IPropertyValidator GetValidator(ValidatorType type, List<object> parameters)
        {
            IPropertyValidator result = (IPropertyValidator)Activator.CreateInstance(Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => typeof(IPropertyValidator).IsAssignableFrom(t))
                .FirstOrDefault(t => ((ValidatorAttribute)Attribute.GetCustomAttribute(t, typeof(ValidatorAttribute)))?.Type == type));

            result.Initialize(parameters);

            return result;
        }

        private IPropertyConverter GetConverter(ConverterType type)
        {
            return (IPropertyConverter)Activator.CreateInstance(Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => typeof(IPropertyConverter).IsAssignableFrom(t))
                .FirstOrDefault(t => ((ConverterAttribute)Attribute.GetCustomAttribute(t, typeof(ConverterAttribute)))?.Type == type));
        }
    }
}
