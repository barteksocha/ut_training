﻿using UnitTestsTraining.Figures.Attributes.Converters;
using UnitTestsTraining.Figures.Attributes.Validators;

namespace UnitTestsTraining.Figures.Attributes
{
    internal class FigureProperty : IFigureProperty
    {
        private readonly IFigure _figure;

        private readonly IPropertyConverter _converter;
        private readonly IPropertyValidator[] _validators;
        
        public FigureProperty(IFigure figure, IPropertyConverter converter, IPropertyValidator[] validators, string name)
        {
            _figure = figure;
            _converter = converter;
            _validators = validators;
            Name = name;
        }

        public string Name { get; }

        public string Value
        {
            get => GetPropertyValueAsString();
            set => SetPropertyValue(value);
        }

        private string GetPropertyValueAsString()
        {
            object propertyValue = _figure.GetType().GetProperty(Name).GetValue(_figure, null);
            return _converter.ConvertToString(propertyValue);
        }

        private void SetPropertyValue(string rawValue)
        {
            object newValue = null;
            if (_converter.Convert(rawValue, out newValue))
            {
                for (int i = 0; i < _validators.Length; ++i)
                {
                    newValue = _validators[i].ValidatePropertyValue(newValue);
                }

                _figure.GetType().GetProperty(Name).SetValue(_figure, newValue);
            }
        }
    }
}
