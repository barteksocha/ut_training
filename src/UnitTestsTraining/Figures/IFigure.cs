﻿using System.Drawing;

using UnitTestsTraining.Canvas;

namespace UnitTestsTraining.Figures
{
    public interface IFigure
    {
        string Name { get; set; }

        Color FillColor { get; set; }
        Color BorderColor { get; set; }

        Point Position { get; set; }
        Size Size { get; set; }

        void DrawFigure(ICanvas canvas);
    }
}
