﻿using System;
using System.Drawing;
using UnitTestsTraining.Canvas;
using UnitTestsTraining.Figures.Attributes;
using UnitTestsTraining.Figures.Attributes.Converters;
using UnitTestsTraining.Figures.Attributes.Validators;

namespace UnitTestsTraining.Figures
{
    public class Star : IFigure
    {
        private const int MINIMAL_POINTS_COUNT = 3;
        private const int MAXIMAL_POINTS_COUNT = 100;

        public string Name { get; set; } = nameof(Star);

        public Color FillColor { get; set; } = Color.CornflowerBlue;
        public Color BorderColor { get; set; } = Color.AliceBlue;

        public Point Position { get; set; } = new Point(300, 300);

        public Size Size { get; set; } = new Size(100, 100);

        [CustomProperty("Points count", ConverterType.Integer, ValidatorType.MinMaxValidator, MINIMAL_POINTS_COUNT, MAXIMAL_POINTS_COUNT)]
        public int PointsCount { get; set; } = MINIMAL_POINTS_COUNT;

        [CustomProperty("Indent", ConverterType.Float, ValidatorType.MinMaxValidator, 0.0f, 1.0f)]
        public float Indent { get; set; } = 0.5f;

        public void DrawFigure(ICanvas canvas)
        {
            if (PointsCount >= MINIMAL_POINTS_COUNT)
            {
                Point[] starPoints = new Point[PointsCount * 2 + 1];
                Point middlePoint = new Point(Position.X + Size.Width / 2, Position.Y + Size.Height / 2);

                starPoints[0] = new Point(middlePoint.X, middlePoint.Y + Size.Height / 2);
                starPoints[starPoints.Length - 1] = starPoints[0];

                double indentCoefficient = Math.Cos(Math.PI / PointsCount);
                
                for (int i = 1; i < PointsCount; ++i)
                {
                    int xValue = Convert.ToInt32(Math.Sin(2 * Math.PI * i / PointsCount) * Size.Width / 2) + middlePoint.X;
                    int yValue = Convert.ToInt32(Math.Cos(2 * Math.PI * i / PointsCount) * Size.Height / 2) + middlePoint.Y;
                    starPoints[2 * i] = new Point(xValue, yValue);
                }

                for (int i = 0; i < PointsCount; ++i)
                {
                    int xValue = Convert.ToInt32(Indent * indentCoefficient * Math.Sin(2 * Math.PI * (i + 0.5f) / PointsCount) * Size.Width / 2) + middlePoint.X;
                    int yValue = Convert.ToInt32(Indent * indentCoefficient * Math.Cos(2 * Math.PI * (i + 0.5f) / PointsCount) * Size.Height / 2) + middlePoint.Y;
                    starPoints[2 * i + 1] = new Point(xValue, yValue);
                }

                for (int i = 1; i < starPoints.Length; ++i)
                {
                    canvas.DrawLine(BorderColor, starPoints[i - 1], starPoints[i]);
                }

                canvas.FillPolygon(FillColor, starPoints);
            }
        }
    }
}
