﻿using System.Drawing;
using UnitTestsTraining.Canvas;

namespace UnitTestsTraining.Figures
{
    internal class Square : IFigure
    {
        public string Name { get; set; } = nameof(Square);

        public Color FillColor { get; set; } = Color.Red;
        public Color BorderColor { get; set; } = Color.Green;

        public Point Position { get; set; } = new Point(30, 30);
        public Size Size { get; set; } = new Size(50, 40);

        public void DrawFigure(ICanvas canvas)
        {
            Point point1 = Position;
            Point point2 = new Point(point1.X + Size.Width, point1.Y);
            Point point3 = new Point(point1.X, point1.Y + Size.Height);
            Point point4 = point1 + Size;

            canvas.FillRectangle(FillColor, Position, Size);
            canvas.DrawLine(BorderColor, point1, point2);
            canvas.DrawLine(BorderColor, point1, point3);
            canvas.DrawLine(BorderColor, point2, point4);
            canvas.DrawLine(BorderColor, point3, point4);
        }
    }
}
