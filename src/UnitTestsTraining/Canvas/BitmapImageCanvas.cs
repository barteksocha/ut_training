﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace UnitTestsTraining.Canvas
{
    public class BitmapImageCanvas : ICanvas
    {
        private Bitmap _bitmap;

        public BitmapImageCanvas(Bitmap bitmap)
        {
            _bitmap = bitmap;
        }

        public int Width => _bitmap.Width;

        public int Height => _bitmap.Height;

        public void DrawLine(Color color, Point start, Point end)
        {
            using (Graphics g = Graphics.FromImage(_bitmap))
            {
                g.DrawLine(new Pen(color), start, end);
            }
        }

        public void DrawPixel(Color color, Point pixel)
        {
            FillRectangle(color, pixel, new Size(1, 1));
        }

        public void FillPolygon(Color color, Point[] polygonPoints)
        {
            using (Graphics g = Graphics.FromImage(_bitmap))
            {
                g.FillPolygon(new SolidBrush(color), polygonPoints, FillMode.Alternate);
            }
        }

        public void FillRectangle(Color color, Point staringPoint, Size size)
        {
            using (Graphics g = Graphics.FromImage(_bitmap))
            {
                g.FillRectangle(new SolidBrush(color), new Rectangle(staringPoint, size));
            }
        }

        public void StartDrawing()
        {
        }

        public void EndDrawing()
        {
        }
    }
}
