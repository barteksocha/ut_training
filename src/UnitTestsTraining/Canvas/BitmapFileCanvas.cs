﻿using System.Drawing;
using System.Drawing.Imaging;

namespace UnitTestsTraining.Canvas
{
    public class BitmapFileCanvas : ICanvas
    {
        private readonly string _outputFilePath;
        private readonly int _width;
        private readonly int _height;

        private Bitmap _bitmap;
        private BitmapImageCanvas _baseCanvas;

        public BitmapFileCanvas(string outputFilePath, int width, int height)
        {
            _outputFilePath = outputFilePath;
            _width = width;
            _height = height;
        }

        public int Width => _baseCanvas.Width;

        public int Height => _baseCanvas.Height;

        public void DrawLine(Color color, Point start, Point end)
        {
             _baseCanvas.DrawLine(color, start, end);
        }

        public void DrawPixel(Color color, Point pixel)
        {
            _baseCanvas.DrawPixel(color, pixel);
        }

        public void FillPolygon(Color color, Point[] polygonPoints)
        {
            _baseCanvas.FillPolygon(color, polygonPoints);
        }

        public void FillRectangle(Color color, Point staringPoint, Size size)
        {
            _baseCanvas.FillRectangle(color, staringPoint, size);
        }

        public void StartDrawing()
        {
            _bitmap = new Bitmap(_width, _height, PixelFormat.Format24bppRgb);
            _baseCanvas = new BitmapImageCanvas((Bitmap)this._bitmap);
        }

        public void EndDrawing()
        {
            _bitmap.Save(_outputFilePath);
        }
    }
}
