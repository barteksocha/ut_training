﻿using System.Drawing;

namespace UnitTestsTraining.Canvas
{
    public interface ICanvas
    {
        int Width { get; }
        int Height { get; }

        void StartDrawing();
        void EndDrawing();

        void DrawPixel(Color color, Point pixel);
        void DrawLine(Color color, Point start, Point end);

        void FillRectangle(Color color, Point staringPoint, Size size);
        void FillPolygon(Color color, Point[] polygonPoints);
    }
}
