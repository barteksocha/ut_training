﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media;
using System.Windows.Shapes;

using UnitTestsTraining.Essentials.Extensions;

using Color = System.Drawing.Color;

namespace UnitTestsTraining.Canvas
{
    public class WindowsShapeCanvas : ICanvas
    {
        private readonly List<Shape> _shapesList;

        public WindowsShapeCanvas(List<Shape> shapesList)
        {
            _shapesList = shapesList;
        }

        public int Width => Convert.ToInt32(_shapesList.Max(s => s.RenderedGeometry.Bounds.X + s.RenderedGeometry.Bounds.Width));

        public int Height => Convert.ToInt32(_shapesList.Max(s => s.RenderedGeometry.Bounds.Y + s.RenderedGeometry.Bounds.Height));

        public void StartDrawing()
        {
            _shapesList.Clear();
        }

        public void EndDrawing()
        {
        }

        public void DrawPixel(Color color, Point pixel)
        {
            _shapesList.Add(new Line
            {
                X1 = pixel.X,
                Y1 = pixel.Y,
                X2 = pixel.X,
                Y2 = pixel.Y,
                StrokeThickness = 1.0,
                Stroke = new SolidColorBrush(color.ToMediaColor())
            });
        }

        public void DrawLine(Color color, Point start, Point end)
        {
            _shapesList.Add(new Line
            {
                X1 = start.X,
                Y1 = start.Y,
                X2 = end.X,
                Y2 = end.Y,
                StrokeThickness = 1.0,
                Stroke = new SolidColorBrush(color.ToMediaColor())
            });
        }

        public void FillRectangle(Color color, Point staringPoint, Size size)
        {
            FillPolygon(color, new []
            {
                staringPoint,
                new Point(staringPoint.X + size.Width, staringPoint.Y),
                new Point(staringPoint.X + size.Width, staringPoint.Y + size.Height),
                new Point(staringPoint.X, staringPoint.Y + size.Height)
            });
        }

        public void FillPolygon(Color color, Point[] polygonPoints)
        {
            _shapesList.Add(new Polygon
            {
                Points = new PointCollection(polygonPoints.Select(p => p.ToWindowsPoint())),
                Fill = new SolidColorBrush(color.ToMediaColor())
            });
    }
    }
}
